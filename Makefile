install:
	cargo build --release
	cp ./target/release/deckster ~/.local/bin/
	chmod +x ~/.local/bin/deckster

uninstall:
	rm ~/.local/bin/deckster

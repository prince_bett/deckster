# Deckster

Spaced repetition in the terminal.
Inspired by Anki and uses a modified SM2 algorithm.

Only supports Unix systems.

![deckster](./assets/screenshot.png)


## Installation


NetBSD installation:
```
pkgin install deckster
```

Manual installation:

Depends on having the rust compiler. Rust can be installed from your
package manager, if not see [here](https://doc.rust-lang.org/book/ch01-01-installation.html).

```sh
git clone https://gitlab.com/prince_bett/deckster
cd deckster
make install
```

Using Cargo :
```sh
cargo install deckster.
```

> Note: make sure ".cargo/bin/" is in your path.

## Usage

```sh
deckster [flag] [arg]

# for help: deckster -h
```



Only text is supported. Users could add urls to Q/A fields
and use their terminal to grab them and open multimedia content.

```
Keybindings
-----------

SPACE   :   Reveal answer.
RETURN  :   Submit rating and move to the next card.
l/RIGHT :   Select next rating option.
h/LEFT  :   Select previous rating option.
q/ESC   :   Exit application.
```

## Creating decks.

Decks are saved in [TOML](https://toml.io/en/) format.
It makes it easier to read and write cards in your favourite editor.
> See `deckster -s`

```toml
# Sample deck about planets.

[[card]]
question = 'How many planets are there ?'
answer = '8'

[[card]]
question = 'Name the inner planets.'
answer = '''
1. Mercury
2. Venus
3. Earth
'''

[[card]]
question = 'What was the first spacecraft to reach Mars'
answer = '''
NASA's Mariner 4.
Launched in 1964.
Entered orbit 1971.
'''

```

## Contributing.

I'd love to hear from you ! Please file issues with
bug reports or ( minimalist ) feature suggestions.
